var _ = require('underscore');
var Sorteo = require('../models/sorteo.js');
var Premio = require('../models/premio.js');
var ronda;

var vivoController = function(app) {
	console.log('VivoControler se cargo');

	// ENRRUTAMOS LA COSA
	app.get('/', function(req, res) {
		Sorteo
			.findOne({estado: "sorteando"})
			.populate('fondo')
			.populate('tipo_sorteo')
			.populate('autoridad')
			.exec(function(err, sorteo){
				if (err){
					res.send(err);
				}
				Premio
					.findOne({sorteo: sorteo})
					.exec(function(err, premio){

						if (sorteo && premio){
							if (!sorteo.rondas)
								ronda = 1
							else {
								if (sorteo.rondas.length >= sorteo.tipo_sorteo.nro_rondas)
									ronda = sorteo.rondas.length;
								else
									ronda = sorteo.rondas.length + 1;
							}

							var s = {
								nro_sorteo: sorteo.nro_sorteo,
								fecha: sorteo.fecha,
								fondo: sorteo.fondo,
								tipo: sorteo.tipo_sorteo.nombre
							};

							res.render('vivo', { 
										title: 'Telebingo Correntino', 
										ronda_sorteando: ronda,
										sorteo: JSON.stringify(s),
										fondo: sorteo.fondo.path,
										rondas: JSON.stringify(sorteo.rondas),
										premios: JSON.stringify(premio.premios)
								}
						  );
						}
						else {
							res.send("No hay sorteo activo. Intente en unos minutos...");
						}
					});
				});
	});

};

module.exports = vivoController;