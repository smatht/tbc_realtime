var _ = require('underscore');
var fs = require('fs');
var async = require('async');
var path = require('path');
var FondoSorteo = require('../models/fondoSorteo');
var Autoridad = require('../models/autoridad');
var TipoSorteo = require('../models/tipoSorteo');
var Sorteo = require('../models/sorteo');
var Premio = require('../models/premio');
var Agencia = require('../models/agencia');

var adminController = function(app) {

	//////////////////////////////////////
	// METODO PARA LEER DATOS DE LA BASE (PENDIENTE)
	//////////////////////////////////////
	// function parallelRead( data ){

	// }

	///////////////////////
	// ENRRUTAMOS LA COSA
	//////////////////////
	app.get('/admin', function(req, res) {

		var error = (req.params.e) ? Number(req.params.e)  : 0;
		
		async.parallel(
		{
			sorteos: function(callback){
				Sorteo
					.find({})
					.sort('-nro_sorteo')
					.populate('tipo_sorteo')
					.populate('fondo')
					.exec(function (err, docs) {
						callback(err, docs);
					});
			},
			fiscalizadores: function(callback){
				Autoridad.find({ 'tipo': 'fiscalizador' }, function (err, docs) {
					callback(err, docs);
				});
			},
			escribanos: function(callback){
				Autoridad.find({ 'tipo': 'escribano' }, function (err, docs) {
					callback(err, docs);
				});
			},      
			tipos: function(callback){
				TipoSorteo.find({}, function (err, docs) {
					callback(err, docs);
				});
			},    
			fondos: function(callback){
				FondoSorteo.find({}, function (err, docs) {
					callback(err, docs);
				});
			},            
		}, 
		function(e, r){
			// var sorteoJson = _.map(r.sorteos, function(sort){
			// 	return sort.toJSON();
			// });
			res.render('admin', { 
				title: 'Administrar juego', 
				operador: 'Matias Sticchi', 
				funcion: ['sorteos', 'nuevo'],
				sorteos: r.sorteos,
				fondos: r.fondos,
				tipos: r.tipos,
				fiscalizadores: r.fiscalizadores,
				escribanos: r.escribanos,
				error: error
			});
	    }
	    );
	});

	app.get('/admin/e/:error', function(req, res) {

		if(req.params.error){
			console.log(req.params.error);
			var error = req.params.error;
		}
		async.parallel(
		{
			sorteos: function(callback){
				Sorteo
					.find({})
					.sort('-nro_sorteo')
					.populate('tipo_sorteo')
					.populate('fondo')
					.exec(function (err, docs) {
						callback(err, docs);
					});
			},
			fiscalizadores: function(callback){
				Autoridad.find({ 'tipo': 'fiscalizador' }, function (err, docs) {
					callback(err, docs);
				});
			},
			escribanos: function(callback){
				Autoridad.find({ 'tipo': 'escribano' }, function (err, docs) {
					callback(err, docs);
				});
			},      
			tipos: function(callback){
				TipoSorteo.find({}, function (err, docs) {
					callback(err, docs);
				});
			},    
			fondos: function(callback){
				FondoSorteo.find({}, function (err, docs) {
					callback(err, docs);
				});
			},            
		}, 
		function(e, r){
			// var sorteoJson = _.map(r.sorteos, function(sort){
			// 	return sort.toJSON();
			// });
			res.render('admin', { 
				title: 'Administrar juego', 
				operador: 'Matias Sticchi', 
				funcion: ['sorteos'],
				sorteos: r.sorteos,
				fondos: r.fondos,
				tipos: r.tipos,
				fiscalizadores: r.fiscalizadores,
				escribanos: r.escribanos,
				error: error
			});
	    }
	    );
	});

	app.get('/admin/sorteo/:sorteo_id', function(req, res) {
		Sorteo.findById(req.params.sorteo_id, function (err, doc) {
			if (err) {
				res.send(err);
			}
	  		doc.remove(function(){
				res.redirect('/admin/');
	  		}); //Removes the document
		});
	});

	app.get('/admin/fondo', function(req, res) {
		FondoSorteo.find({}, function(err, fondoSorteos){
			if (err){
				return res.send(err);
			}
		res.render('admin', { 
		  	title: 'Nuevo fondo', 
		  	operador: 'Matias Sticchi', 
		  	funcion: ['fondos'],
		  	fondos: fondoSorteos
		 });
		});
	});

	app.get('/admin/fondo/:fondo_id', function(req, res) {
		FondoSorteo.findById(req.params.fondo_id, function (err, doc) {
			if (err) {
				res.send(err);
			}
	  		doc.remove(function(){
				res.redirect('/admin/fondo');
	  		}); //Removes the document
		});
	});

	app.get('/admin/autoridad', function(req, res) {
		res.render('admin', { 
		  	title: 'Nueva autoridad', 
		  	operador: 'Matias Sticchi', 
		  	funcion: ['nueva-autoridad']
		 });
	});

	app.get('/admin/asignar-premios/:sorteo', function(req, res){
		
	    Sorteo
				.findById(req.params.sorteo)
				.populate("tipo_sorteo")
				.exec(function(err, sorteo){
					if (err){
						res.send(err);
					}
					Premio.find({ 'sorteo': sorteo }, function (err, premios) {
						var premiosJson;
						if (premios.length > 0){
							premiosJson = _.map(premios[0].premios, function(pre){
								return pre.toJSON();
							});
						}
						else
							premiosJson = "";
						res.render('admin', { 
							title: 'Asignar premios', 
							operador: 'Matias Sticchi', 
							funcion: ['asignar-premios'],
							sorteo: sorteo, 
							premio: JSON.stringify(premiosJson)
						});
					});
				});
		
	});

	app.get('/admin/asignar-ganadores/:sort', function(req, res){

		res.render('admin', { 
			title: 'Asignar ganadores', 
			operador: 'Matias Sticchi', 
			funcion: ['ganadores'],
			sorteo: req.params.sort, 
		});
				
	});



	//////////////////
	// MIDDLEWARES
	/////////////////
	var getSorteo = function(req, res, next){
		var sorteo_id;
		if (req.body.sorteo)
			sorteo_id = req.body.sorteo;
		else {
			if (req.params.sorteo)
				sorteo_id = req.params.sorteo;
		}

		Sorteo.findById(sorteo_id, function(err, doc){
			if (err){
				res.send(err);
			}
			req.sorteo = doc;
			next();
		});
	};

	var getTipoSorteo = function(req, res, next){
		var tipoSorteo_id;
		if (req.body.tipoSorteo)
			tipoSorteo_id = req.body.tipoSorteo;
		else {
			if (req.params.tipoSorteo)
				tipoSorteo_id = req.params.tipoSorteo;
		}

		TipoSorteo.findById(tipoSorteo_id, function(err, doc){
			if (err){
				res.send(err);
			}
			req.tipoSorteo = doc;
			next();
		});
	};
	var getFondoSorteo = function(req, res, next){
		var fondoSorteo_id;
		if (req.body.fondo)
			fondoSorteo_id = req.body.fondo;
		else {
			if (req.params.fondo)
				fondoSorteo_id = req.params.fondo;
		}

		FondoSorteo.findById(fondoSorteo_id, function(err, doc){
			if (err){
				res.send(err);
			}
			req.fondo = doc;
			next();
		});
	};
	var getFiscalizador = function(req, res, next){
		var fiscalizador_id;
		if (req.body.fiscalizador)
			fiscalizador_id = req.body.fiscalizador;
		else {
			if (req.params.fiscalizador)
				fiscalizador_id = req.params.fiscalizador;
		}

		Autoridad.findById(fiscalizador_id, function(err, doc){
			if (err){
				res.send(err);
			}
			req.fiscalizador = doc;
			next();
		});
	};
	var getEscribano = function(req, res, next){
		var escribano_id;
		if (req.body.escribano)
			escribano_id = req.body.escribano;
		else {
			if (req.params.escribano)
				escribano_id = req.params.escribano;
		}

		Autoridad.findById(escribano_id, function(err, doc){
			if (err){
				res.send(err);
			}
			req.escribano = doc;
			next();
		});
	};

	
	app.post('/admin/nuevoSorteo', getTipoSorteo, getFondoSorteo, getFiscalizador, getEscribano, function(req, res){
		
		var sort = {
			nro_sorteo: req.body.nro_sorteo,
			fecha: req.body.fecha,
			tipo_sorteo: req.tipoSorteo,
			estado: req.body.estado,
			fondo: req.fondo,
			autoridad: [req.fiscalizador, req.escribano]
		};


		var sorteo = new Sorteo(sort);
		sorteo.save(function(err){
			console.log(sorteo);
			if(err){
				res.send(err);
			}
			if (req.body.boton == 'crearypremios'){

					res.redirect('/admin/asignar-premios/'+sorteo._id);
			}
			else {
				res.redirect('/admin');
			}
		});
		
	});

	

	// app.get('/admin/get-premios/', function(req, res){
	// 	console.log(req.query.sorteo_id +" "+req.query.ronda);
	// 	Premio
	// 		.findOne({sorteo: req.params.sorteo})
	// 		.exec(function(err, premio){
	// 			if (err){
	// 				res.send(err);
	// 			}
	// 			res.render('admin', { 
	// 							title: 'Asignar premios', 
	// 							operador: 'Matias Sticchi', 
	// 							funcion: ['asignar-premios'],
	// 							sorteo: sorteo
	// 						});
	// 		});
	// });

	app.post('/admin/post-premio', getSorteo, function(req, res){
		var sorteo = req.sorteo;
		var premio = req.body.premios;
		var sorteoPremio = {
			sorteo: sorteo,
			premios: [premio]
		}
		Premio.findOne({sorteo: sorteo}, function(err, premio){
			if (err)
				res.send(error);
			if (premio){
				for (var p=0; p < premio.premios.length; p++)
					if (premio.premios[p].nroRonda == req.body.premios.nroRonda){
						premio.premios[p].premioLinea.monto = req.body.premios.premioLinea.monto;
						premio.premios[p].premioLinea.especie = req.body.premios.premioLinea.especie;
						premio.premios[p].premioLinea.bolillaAcumulado = req.body.premios.premioLinea.bolillaAcumulado;
						premio.premios[p].premioLinea.montoAcumulado = req.body.premios.premioLinea.montoAcumulado;
						premio.premios[p].premioLinea.especieAcumulado = req.body.premios.premioLinea.especieAcumulado;

						premio.premios[p].premioBingo.monto = req.body.premios.premioBingo.monto;
						premio.premios[p].premioBingo.especie = req.body.premios.premioBingo.especie;
						premio.premios[p].premioBingo.bolillaAcumulado = req.body.premios.premioBingo.bolillaAcumulado;
						premio.premios[p].premioBingo.montoAcumulado = req.body.premios.premioBingo.montoAcumulado;
						premio.premios[p].premioBingo.especieAcumulado = req.body.premios.premioBingo.especieAcumulado;

						premio.save(function(err, prem){
							console.log(prem);
							if (err){
								res.send({
									clase: 'alert-danger', 
									encabezado: 'Error!',
									msg: 'Ocurri&oacute; un error inesperado.'
									
								});
							}
							res.send({
								clase: 'alert-success', 
								encabezado: 'Ok!',
								msg: 'Ronda modificada correctamente.',
								refresh: true
							});
						})

					}
					else
					{
					Premio.findByIdAndUpdate(premio._id, 
						{$push: { premios : sorteoPremio.premios[0] }},
						{safe: true, upsert:true}, 
						function(err, data) { 
							if (err){
								res.send({
									clase: 'alert-danger', 
									encabezado: 'Error!',
									msg: 'Ocurri&oacute; un error inesperado.'

								});
							}
							res.send({
								clase: 'alert-success', 
								encabezado: 'Ok!',
								msg: 'Ronda cargada correctamente.',
								refresh: true
							});
						});
					}
			}
			else{
				console.log("Entro por nuevo sorteo");
				var nuevoPremio = new Premio(sorteoPremio);
				nuevoPremio.save(function(err){
					if (err){
						res.send({
							clase: 'alert-danger', 
							encabezado: 'Error!',
							msg: 'Ocurri&oacute; un error inesperado.'
							
						});
					}
					res.send({
					clase: 'alert-success', 
					encabezado: 'Ok!',
					msg: 'Ronda cargada correctamente.',
					refresh: true
				});
				});
			}

		});
				
	});

	app.post('/admin/upload/fondo', function(req, res) {
		var fondoSorteo = require('../models/fondoSorteo');
		// console.log(req.body);
		console.log(req.files);
		fs.readFile(req.files.image.path, function (err, data) {
			var imagenNombre = req.files.image.name
			var pathImagen = "/images/fondos/" + imagenNombre
			/// If there's an error
			if(!imagenNombre){

				console.log("Hubo un error")
				res.redirect("/admin/fondo");
				res.end();

			} else {

				var newPath = './public' + pathImagen;

			 	/// write file to uploads/fullsize folder
			 	fs.writeFile(newPath, data, function (err) {

			 	var nuevoFondo = new fondoSorteo({
			 		descripcion: req.body.descripcion,
			 		path: pathImagen});

			 	nuevoFondo.save();

			  	/// let's see it
			  	res.redirect("/admin/fondo/");

			  });
			}
		});
	});

	app.post('/admin/upload/ganadores', function(req, res) {
		var Ganadores = require('../models/ganador');
		fs.readFile(req.files.ganadores.path, function (err, data) {
			if(err){

				console.log("Hubo un error")
				res.redirect("/admin");
				res.end();

			} else {
				Sorteo
					.findById(req.body.sorteo)
					.exec(function (err, sort) {
						var datosGanadores = data.toString('utf-8');
						if (sort.nro_sorteo == Number(datosGanadores.substring(0,4)))
						{
							var datosGanadoresArray = datosGanadores.split('\r\n');
							var ganador = {};
							ganador.sorteo = sort;
							ganador.ganadores = new Array();
							for (i in datosGanadoresArray){
								!function(i, ganador){
									if (datosGanadoresArray[i].length < 5)
										return
									else {
										Agencia
											.find({nroAgencia: Number(datosGanadoresArray[i].substring(10,13))})
											.exec(function (err, ag) {
												gan = {
													nroBillete: datosGanadoresArray[i].substring(4,10),
													agencia: ag,
													montoPremio: datosGanadoresArray[i].substring(13,20)+'.'+datosGanadoresArray[i].substring(20,22),
													premioA: datosGanadoresArray[i].substring(22,23),
												};
												ganador.ganadores[ganador.ganadores.length] = gan;
												if (ganador.ganadores.length >= datosGanadoresArray.length - 1)
													console.log(ganador); 
											});
									}
								}(i, ganador)
							}
						}
						else {
							res.redirect('/admin/e/101');
						}
						
					});

				
			  	res.redirect("/admin");

			}
		});
	});

	app.post('/admin/autoridad/nuevo', function(req, res){
		var Autoridad = require('../models/autoridad');
		var aut = {
			nombre: req.body.nombre,
			apellido: req.body.apellido,
			documento: req.body.dni,
			tipo: req.body.tipoAutoridad
		};

		var autoridad1 = new Autoridad(aut);
		autoridad1.save();
		
		res.redirect("/admin/");
	});

	// Cambio de estado
	app.post('/admin/cambio-estado/:a', function(req, res){
		console.log(req.body.id);

		if (req.params.a == 'sorteando'){
			Sorteo.count({estado: 'sorteando'}, function(err, num){
				if(num >= 1)
					res.redirect('/admin/e/100');
				else {
					Sorteo.findById(req.body.id, function(err, sorteo){
						if (err){
							return res.send(err);
						}
						sorteo.estado = 'sorteando';
						sorteo.save();
					});
					res.redirect('/carga/'+req.body.id);
				}
			});
		}

		
	});

	//REAL TIME

	// app.io.route('salio-linea', function(req){
	// 	console.log('La re putisima madre');
	//     req.io.broadcast('actualizacion-linea-bingo', {
	//         content: 'SALIO LINEA!'
	//     });
	// });
	// app.io.route('salio-bingo', function(req){
	//     req.io.broadcast('actualizacion-linea-bingo', {
	//         content: 'SALIO BINGO!'
	//     });
	//});
};

module.exports = adminController;