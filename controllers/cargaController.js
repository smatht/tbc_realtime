var _ = require('underscore');
var Sorteo = require('../models/sorteo');
// var Premio = require('../models/premio');

var id_sorteo;
var nro_ronda = 0;
// Array de objetos que contendran rondas sorteadas
var arrayRondas = new Array();
// Array para guardar los numero que juegan por linea
var arrayLineaEnJuego = new Array();
// Array para guardar los numero que juegan por bingo
var arrayBingoEnJuego = new Array();
// para saber si estamos jugando por linea o por bingo
var linea_bingo = '-';


// El estado de la carga representa "un momento" con respecto a un sorteo en curso. 
// nroRonda = 0 | 1 | 2 | 3... Que ronda se esta jugando (0 = no se empezo ninguna ronda)
// contenido = un objeto con dos arrays de numero, uno para linea y otro para bingo
var estadoCarga = {
	nroRonda: 0,
	contenido: {
		linea: arrayLineaEnJuego,
		bingo: arrayBingoEnJuego
	}
};


var cargaController = function(app) {

	///////////////////////
	// ENRRUTAMOS LA COSA
	//////////////////////
	app.get('/carga/queJugamos', function(req, res){
		res.send({juego: linea_bingo});
	});

	app.get('/carga/cierre-ronda', function(req, res){
				if (linea_bingo == '-' && arrayLineaEnJuego.length > 0 && arrayBingoEnJuego.length > 0){
					estadoCarga.nroRonda = nro_ronda;
					estadoCarga.contenido.linea = arrayLineaEnJuego;
					estadoCarga.contenido.bingo = arrayBingoEnJuego;
					Sorteo.findById(id_sorteo)
						.populate('tipo_sorteo')
						.exec(function(err, sorteo){
							if (err){
								return res.send(err);
							}
							sorteo.rondas.push(estadoCarga);
							sorteo.save(function(err){
								if (err) return res.send(err);
									req.io.broadcast('actualizar-ronda');
								
								nro_ronda += 1;
								if (nro_ronda <= sorteo.tipo_sorteo.nro_rondas ){
									linea_bingo = 'linea';
									arrayLineaEnJuego = new Array();
									arrayBingoEnJuego = new Array();
									res.render('carga', { title: 'Telebingo Correntino | Estamos sorteando...', ronda_sorteando: nro_ronda, s: sorteo });
								}
								else {
									sorteo.estado = 'sorteado';
									sorteo.save();
									res.redirect('/admin');
								}
							});
						});
				}
				else 
					res.redirect('/carga/'+id_sorteo);
			});

	app.get('/carga/:sorteo_id', function(req, res) {

		id_sorteo = req.params.sorteo_id;
		Sorteo.findById(req.params.sorteo_id, function(err, sorteo){
			if (err){
				return res.send(err);
			}
			if (sorteo.rondas.length == 0 && nro_ronda < 1){
				nro_ronda = sorteo.rondas.length + 1
				linea_bingo = 'linea';
				
				arrayLineaEnJuego = new Array();
				arrayBingoEnJuego = new Array();
				
				sorteo.rondas = new Array();
				sorteo.save();
				res.render('carga', { title: 'Telebingo Correntino | Estamos sorteando...', ronda_sorteando: nro_ronda, s: sorteo });
			}
			else {
				res.render('carga', { title: 'Telebingo Correntino | Estamos sorteando...', ronda_sorteando: nro_ronda, s: sorteo });
			}
			
		});
	});


	// Realiza el cierre de lineas y bingos
	app.post('/carga', function(req, res){
		res.type('json');
		if(req.body.queSalio == 'linea'){
			if (linea_bingo == 'linea' && arrayLineaEnJuego.length >= 5){
				linea_bingo = 'bingo';
				req.io.broadcast('actualizacion-linea-bingo', {
					content: 'linea',
					cantidad: req.body.cantidad
				});
	  			res.send({
					clase: 'alert-success', 
					encabezado: 'Ok!',
					msg: 'Linea cerrada.'
				});	
			}
			else {
				res.send({
					clase: 'alert-warning', 
					encabezado: 'Error!',
					msg: 'Linea ya esta cerrada o no hay suficientes bolillas sorteadas.'
				});	
			}
		}
  		else {
  			if(req.body.queSalio == 'bingo'){
  				console.log("Lineabingo: "+linea_bingo+" Arraybingo: "+arrayBingoEnJuego);
  				if (linea_bingo == 'bingo' && arrayBingoEnJuego.length >= 10){
	  				linea_bingo = '-';
	  				req.io.broadcast('actualizacion-linea-bingo', {
						content: 'bingo',
						cantidad: req.body.cantidad
					});
	  				res.send({
						clase: 'alert-info', 
						encabezado: 'Ok!',
						msg: 'Bingo cerrado.'
					});
	  			}
	  			else {
	  				res.send({
						clase: 'alert-warning', 
						encabezado: 'Error!',
						msg: 'Bingo ya esta cerrado o no hay suficientes bolillas sorteadas.'
					});
	  			}
  			}
  		}
	});

	// Solicitud de los clientes en vivo
	app.post('/carga/necesito-datos', function(req, res){
		//res.type('json');
		res.send({
			linea: arrayLineaEnJuego,
			bingo: arrayBingoEnJuego
		});
	});



	///////////////////////
	// Fin rutas
	

	
	//REAL TIME
	/////////////////

	// carga de numeros
	app.io.route('carga', function(req){
		if (linea_bingo == 'linea' || linea_bingo == 'bingo'){
			if (linea_bingo == 'linea')
				arrayLineaEnJuego.push(req.data.content);
			else {
				arrayBingoEnJuego.push(req.data.content);
			}
			req.io.broadcast('actualizar-grilla', {
				content: req.data.content
			});
		}
		else
			req.io.emit('msg-carga', {
				clase: 'alert-danger', 
				encabezado: 'Error!',
				msg: 'No se puede seguir cargando numeros. <dfn>Ronda ya cargada</dfn>'
			});
	});
	

	///////////////////////
	// Fin real time

}

module.exports = cargaController;