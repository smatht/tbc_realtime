var express = require('express.io'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    swig = require('swig'),
    _ = require('underscore');

// var vivo = require('./routes/vivo');
// var carga= require('./routes/carga');
// var admin= require('./routes/administracion');


var app = express();
app.http().io();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', swig.renderFile);
app.set('view engine','html');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/images/logo.ico'));
app.use(logger('dev'));
// app.use(bodyParser.json());
app.use(express.bodyParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));


///////////////////////////////
// ENRUTAMOS LA COSA
/////////////////////////////
// app.use('/', vivo);
// app.use('/carga', carga);
// app.use('/admin', admin);

//Controlers
var vivoController = require('./controllers/vivoController');
vivoController(app);

var cargaController = require('./controllers/cargaController');
cargaController(app);

var adminController = require('./controllers/administracionController');
adminController(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});




module.exports = app;
