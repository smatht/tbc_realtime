var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

//////////////
// Schema
//////////////
var ganadorSchema = new Schema({
	sorteo: { type: Schema.ObjectId, ref: 'Sorteo' },
	ganadores: [{
		nroBillete: Number,
		agencia: { type: Schema.ObjectId, ref: 'Agencia' },
		montoPremio: Number,
		premioA: String
	}]
	
});

var Ganador = mongooseConected.model('Ganador', ganadorSchema);

module.exports = Ganador;