var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

var tipoSorteoSchema = new Schema({
	nombre: String,
	descripcion: String,
	nro_rondas: Number
});

var TipoSorteo = mongooseConected.model('TipoSorteo', tipoSorteoSchema);

module.exports = TipoSorteo;
