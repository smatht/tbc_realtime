var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

//////////////
// Schema
//////////////
var autoridadSchema = new Schema({
	nombre: String,
	apellido: String,
	documento: Number,
	tipo: String
});

var Autoridad = mongooseConected.model('Autoridad', autoridadSchema);

module.exports = Autoridad;