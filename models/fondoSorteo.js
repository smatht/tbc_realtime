var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

var fondoSorteoSchema = new Schema({
	nombre: String,
	descripcion: String,
	path: String
});

var FondoSorteo = mongooseConected.model('FondoSorteo', fondoSorteoSchema);

module.exports = FondoSorteo;
