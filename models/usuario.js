var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

var usuarioSchema = new Schema({
	nombre: String,
	pass: String
});

var Usuario = mongooseConected.model('usuario', usuarioSchema);

module.exports = Usuario;
