var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

//////////////
// Schema
//////////////
var premioSchema = new Schema({
	sorteo: { type: Schema.ObjectId, ref: 'Sorteo' },
	premios: [{
		nroRonda: Number,
		premioLinea: {
                     monto: Number,
                     especie: String,
                     bolillaAcumulado: Number,
                     montoAcumulado: Number,
                     especieAcumulado: String
                  	 },
        premioBingo: {
                     monto: Number,
                     especie: String,
                     bolillaAcumulado: Number,
                     montoAcumulado: Number,
                     especieAcumulado: String
                  	 }
	}]
	
});

var Premio = mongooseConected.model('Premio', premioSchema);

module.exports = Premio;