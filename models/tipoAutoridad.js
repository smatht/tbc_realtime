var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

//////////////
// Schema
//////////////
var tipoAutoridadSchema = new Schema({
	descripcion: String

});

var TipoAutoridad = mongooseConected.model('TipoAutoridad', tipoAutoridadSchema);

module.exports = TipoAutoridad;