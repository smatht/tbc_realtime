var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

//////////////
// Schema
//////////////
var agenciaSchema = new Schema({
	nroAgencia: Number,
	descripcion: String,
	localidad: String
	
});

var Agencia = mongooseConected.model('Agencia', agenciaSchema);

module.exports = Agencia;