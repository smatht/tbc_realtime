var mongooseConected = require('./conectionMongoose'),
	Schema = mongooseConected.Schema;

//////////////
// Schema
//////////////
var sorteoSchema = new Schema({
	nro_sorteo: Number,
	fecha: Date,
	tipo_sorteo: { type: Schema.ObjectId, ref: 'TipoSorteo' },
	estado: String,
	fondo: {
		type: Schema.ObjectId,
		ref: 'FondoSorteo'
	},
	autoridad: [{
		type: Schema.ObjectId,
		ref: 'Autoridad'
	}],
	rondas: []
});

// Metodo para cambiar estado del sorteo
// sorteoSchema.methods.changeState = function (estado, cb) {
//   return this.model('Sorteo').find({ type: this.type }, cb);
// }

var Sorteo = mongooseConected.model('Sorteo', sorteoSchema);

module.exports = Sorteo;