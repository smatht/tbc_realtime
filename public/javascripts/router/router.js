TBC.Router = Backbone.Router.extend({
  routes: {
    "": "index"
    
  },

  initialize: function () {
    this.current = {};
    this.rondaActual;
    this.sorteo = new TBC.Models.Sorteo();
    this.lineaBingo = new TBC.Models.LineaBingo();
    this.rondas = new TBC.Collections.Rondas();
    this.premios = new TBC.Collections.Premios();
    this.numerosSorteados = new TBC.Collections.NumerosSorteados();
    this.datosSorteo = new TBC.Views.Sorteo({ model: this.sorteo });
    this.premiosView = new TBC.Views.Premios({ collection: this.premios });
    this.premioABolillaView = new TBC.Views.PremioABolilla({ collection: this.premios });
    this.lineaBingoView = new TBC.Views.SalioLineaBingo({ model: this.lineaBingo });
    this.rondasView = new TBC.Views.Rondas({ collection: this.rondas });
    this.grillaView = new TBC.Views.GrillaNumeros({ collection: this.numerosSorteados });
    
    Backbone.history.start();
  },

  index: function () {
    ////////////////////////////////////////
    // Cargo datos en la tabla del sorteo
    this.cargaSorteo();
    ////////////////////////////////////////

    var strRonda = $('#_ronda').html();
    this.rondaActual = Number(strRonda);

    //////////////////////////////////////////////
    // Cargo array de premios
    var strPremios = $('#_premios').html();
    var premios = JSON.parse(strPremios);
    for (var p in premios) {
      this.cargaPremio(premios[p]);
      if(Number(p)+1 >= premios.length){
        this.updatePremio();
        this.cargaGrilla();
      }
    }
    //////////////////////////////////////////////

    //////////////////////////////////////////////
    // Cargo array de rondas ya sorteadas
    var rondas = JSON.parse($('#_rondas').html());
    for (var r in rondas) {
      this.cargaRondaSorteada(rondas[r]);
    }
    //////////////////////////////////////////////
  },

  cargaSorteo: function(){
    var strSorteo = $('#_sorteo').html();
    var sorteo = JSON.parse(strSorteo);

    this.sorteo.set({
      nro_sorteo: sorteo.nro_sorteo,
      fecha: sorteo.fecha,
      fondo: sorteo.fondo,
      tipo: sorteo.tipo
    });
  },

  cargaPremio: function(premio){
    this.premios.add(new TBC.Models.Premio({
      nro_ronda: premio.nroRonda,
      premio_linea: premio.premioLinea,
      premio_bingo: premio.premioBingo
    }));
  },

  cargaRondaSorteada: function(ronda){
    this.rondas.add(new TBC.Models.Ronda({
      nro_ronda: ronda.nroRonda,
      sorteado_linea: ronda.contenido.linea,
      sorteado_bingo: ronda.contenido.bingo
    }));
  },


  updatePremio: function () {
    self = this;
    // var strRonda = $('#_ronda').html();
    // var ronda = Number(strRonda);

    $.ajax({
            url: '/carga/queJugamos',
            success: function(data){
              var lineaBingo = data.juego;
              self.premiosView.render(self.rondaActual, lineaBingo);
              if (lineaBingo == 'bingo')
                self.premioABolillaView.render(self.rondaActual);
            },
            dataType: "json"
          }).done(function(){
            self.premiosView.$el.show();
          });
  },

  cargaGrilla: function () {
    $.ajax({
      type: "POST",
      url: '/carga/necesito-datos',
      data: {},
      success: function(data){
        for (x in data.linea){
          // $('#b'+data.linea[x]).html(data.linea[x]);
          numero = {content: data.linea[x]};
          TBC.app.numerosSorteados.add(new TBC.Models.NumeroSorteado(numero));
        };
        for (y in data.bingo){
          // $('#b'+data.bingo[y]).html(data.bingo[y]);
          numero = {content: data.bingo[y]};
          TBC.app.numerosSorteados.add(new TBC.Models.NumeroSorteado(numero));
        };
      },
      dataType: "json"
    });
  },

 });