TBC.Views.Sorteo = Backbone.View.extend({
  el: $("#enc-sort"),

  events: {
    'click .action.icon-add': 'select',
    'click .action.icon-love': 'love',
    'click .action.icon-share': 'share'
  },

  initialize: function () {
    this.listenTo(this.model, 'change', this.render, this);
  },

  render: function () {
    var sorteo = this.model.toJSON();
    var nroSort = sorteo.nro_sorteo;
    var fecha = new Date(sorteo.fecha);
    var tipo = sorteo.tipo;
    this.$el.html('<div class="col-xs-4 col-md-4 dat-sort">Nº '+nroSort+'</div>'+
                    '<div class="col-xs-4 col-md-4 dat-sort">'+eval(fecha.getDate()+1)+'/'+eval(fecha.getMonth()+1)+'/'+fecha.getFullYear()+'</div>'+
                    '<div class="col-xs-4 col-md-4 dat-sort">'+tipo+'</div>');
    return this;
  },

  select: function () {
    
  },

  love: function () {
  },

  share: function () {

  }
});
