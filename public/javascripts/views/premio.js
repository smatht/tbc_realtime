TBC.Views.Premio = Backbone.View.extend({

  initialize: function () {
    // this.listenTo(this.model, "change", this.render, this);
  },

  render: function (lineaBingo) {
    var premios = this.model.toJSON()
    var html;

    if (lineaBingo == 'linea' || lineaBingo == '-')
      html = "Estamos jugando a LINEA por $" + premios.premio_linea.monto;
    else
      html = "Estamos jugando a BINGO por $" + premios.premio_bingo.monto;

    this.$el.html(html);
    return this;
  },

  
});