TBC.Views.GrillaNumeros = Backbone.View.extend({

  initialize: function () {
    this.listenTo(this.collection, "add", this.addOne, this);
  },

  render: function () {
    this.collection.forEach(this.addOne, this);
  },

  addOne: function (numero) {
  	var num = numero.toJSON();
    var grillaNumero = new TBC.Views.GrillaNumero({ model: numero });

    this.$el = $('#b'+num.content);

    this.$el.html(grillaNumero.render().$el.html());
    
  }

});
