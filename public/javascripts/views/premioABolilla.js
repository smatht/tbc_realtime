TBC.Views.PremioABolilla = Backbone.View.extend({
  el: $('#acumulados'),

  initialize: function () {
    //  this.listenTo(this.collection, "add", this.addOne, this);
  },

  render: function (ronda) {
    var premio;
    var acumulado = this.collection.findWhere({nro_ronda: ronda}).toJSON();
    if (acumulado.premio_bingo.bolillaAcumulado)
    {
      if (acumulado.premio_bingo.especieAcumulado)
        premio = acumulado.premio_bingo.especieAcumulado;
      else
        premio = '$'+String(acumulado.premio_bingo.montoAcumulado);

      this.$el.html('Bolilla predeterminada '+acumulado.premio_bingo.bolillaAcumulado+
        '<br />'+premio);
      this.$el.show();
    }
  },

  hide: function () {
    this.$el.hide( "slow" );
  },

});
