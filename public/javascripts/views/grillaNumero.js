TBC.Views.GrillaNumero = Backbone.View.extend({

  initialize: function () {
    // this.listenTo(this.model, "change", this.render, this);
  },

  render: function () {
    var numero = this.model.toJSON()

    this.$el.html(numero.content);
    return this;
  },

  
});