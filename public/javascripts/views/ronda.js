TBC.Views.Ronda = Backbone.View.extend({

  tagName: 'div',
  className: 'col-md-7 table-responsive',

  events: {
    'click': 'navigate'
  },

  initialize: function () {
    this.listenTo(this.model, "change", this.render, this);
  },

  render: function () {
    var ronda = this.model.toJSON();

    var linea = ronda.sorteado_linea;
    var bingo = ronda.sorteado_bingo;
    var nroRonda = ronda.nro_ronda;

    var headerHTML = '<h3 class="h_sorteadas">Ronda '+nroRonda+'</h3>';
    var lineaHTML = '<h4 class="sh_sorteadas">Linea</h4>';
    var bingoHTML = '<h4 class="sh_sorteadas">Bingo</h4>';
    var fixLineaBingo = '<br /> <br />  <br />';
    var bolillasLinea = $('<div class="bolillas">');
    var bolillasBingo = $('<div class="bolillas">');

    for (var l in linea){
    	bolillasLinea.append('<div class="bolillaSmall">'+linea[l]+'</div>');
    	bolillasBingo.append('<div class="bolillaSmall">'+linea[l]+'</div>');
    	if(Number(l)+1 >= linea.length){
	        for (var b in bingo){
	        	bolillasBingo.append('<div class="bolillaSmall">'+bingo[b]+'</div>');
	        	if(Number(b)+1 >= bingo.length){
	        		var html = headerHTML + lineaHTML + bolillasLinea[0].outerHTML + bingoHTML + bolillasBingo[0].outerHTML;
	        		this.$el.html(html);
    				return this;
	        	}
	        }
      	}
    }

    
    
  },

  navigate: function () {
    alert("que quiere mi rey?");
  }

});

