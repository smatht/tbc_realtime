TBC.Views.Rondas = Backbone.View.extend({
  el: $('.sorteadas'),

  initialize: function () {
    this.listenTo(this.collection, "add", this.addOne, this);
  },

  render: function () {
    this.collection.forEach(this.addOne, this);
  },

  addOne: function (ronda) {
    var rondaView = new TBC.Views.Ronda({ model: ronda });
    this.$el.append(rondaView.render().el);

  }

});
