TBC.Views.Premios = Backbone.View.extend({
  el: $('#premios'),

  initialize: function () {
    //  this.listenTo(this.collection, "add", this.addOne, this);
  },

  render: function (ronda, lineaBingo) {
    var premio = this.collection.findWhere({nro_ronda: ronda});
    this.addOne(premio, lineaBingo);
  },

  addOne: function (premio, lineaBingo) {
    var premioView = new TBC.Views.Premio({ model: premio });
    this.$el.html(premioView.render(lineaBingo).$el.html());
  }

});
