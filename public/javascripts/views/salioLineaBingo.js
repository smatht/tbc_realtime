TBC.Views.SalioLineaBingo = Backbone.View.extend({
  el: $('#linea-bingo'),

  initialize: function () {
    this.listenTo(this.model, "change", this.render, this);
  },

  render: function () {
    var lb = this.model.toJSON()
    this.$el.css('opacity',1);
    this.$el.html('SALIO '+lb.content.toUpperCase()+'!!<br />'+lb.cantidad+' GANADOR/ES');
  },

  hide: function () {
  	this.$el.animate({ "opacity": "0" }, "slow" );
  },

  
});