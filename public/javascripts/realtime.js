$(document).ready(function(){
	//nos conectamos al server
	window.io = io.connect();
	var ronda = TBC.app.rondaActual;
    var bolillaRonda = TBC.app.premios.findWhere({nro_ronda: 1}).toJSON().premio_bingo.bolillaAcumulado;

	io.on('actualizar-grilla', function(data){
        // $('#b'+data.content).html(data.content);
        var numeroModel = new TBC.Models.NumeroSorteado(data);
        TBC.app.numerosSorteados.add(numeroModel);
        if (TBC.app.numerosSorteados.length >= bolillaRonda)
        	TBC.app.premioABolillaView.hide();
        
	});

	io.on('actualizar-ronda', function(){
        location.reload();
	});

	io.on('actualizacion-linea-bingo', function(data){
   //      var lineaBingo = data.content.toUpperCase();
   //      $('#linea-bingo').css('opacity',1);
   //      $('#linea-bingo').html('SALIO '+data.content+'!!<br />'+data.cantidad+' GANADOR/ES');
   //      // $('#premios').fadeOut();
   //      if (data.content == 'linea'){
			// setTimeout(function(){
			// 	recargaPremio();
			// 	$('#linea-bingo').css('opacity', 0);
			// }, 10000);	
   //      }
   		TBC.app.lineaBingo.set({content: data.content, cantidad: data.cantidad});
   		if (data.content == 'linea') {
   		setTimeout(function(){
				TBC.app.updatePremio();
				setTimeout(function(){
					TBC.app.lineaBingoView.hide();
				}, 10000);
			}, 10000);
   		}
		
	});

	


});