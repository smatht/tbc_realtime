// Con esto creamos un Namespace para poder usarlo externamente
EnVivo = {};

// Modelo backbone
EnVivo.Sorteo = Backbone.Model.extend({});

// Vista backbone
EnVivo.SorteoView = Backbone.View.extend({
// Propiedades de la vista
	tagName: 'li',
	className: 'item',
//	template: Handlebars.compile($('#sorteo-template').html()),

// Funciones de la vista
	render: function (){
		var sorteo = this.model;
		var numero = sorteo.get('nro_sorteo');
		var fecha = sorteo.get('fecha');
		fecha = new Date(fecha);
//		var html = this.template(sorteo.toJSON());

		this.$el.html('<span>Sorteo: '+ numero +'</span> - <span>Fecha: '+ eval(fecha.getDate()+1) + '-' + eval(fecha.getMonth()+1) + '-' + fecha.getFullYear() +'</span>');
	}
});




/*
	EJEMPLO DE MEJORANDOELHUEVO
// Con esto creamos un Namespace para poder usarlo desde la consola del explorador
Sfotipy = {};

Sfotypy.Song = Backbone.Model.extend({});

// Una coleccion es una lista de modelos backbone
Sfotipy.Songs = Backbone.Collection.extend({
	model: Sfotypy.Song
});

Sfotipy.SongView = Backbone.View.extend({
	events: {
		// 'EVENTO SELECTOR': 'FUNCION'
		'click .action.icon-add': 'add'
	},
	tagName: 'li',
	className: 'item border-bottom',

	// Se ejecuta cuando se instancia una vista o un modelo
	initialize: function(){
		// Forma de escuchar un evento
		// listenTo(OBJETO_a_escuchar, EVENTO_a_escuchar, METODO_a_ejecutar, ESCOPE)
		this.listenTo(this.model, 'change', this.render, this);
	},

	render: function (){
		var song = this.model,
		var name = song.get('name');
		var author = song.get('author');

		this.$el.html("<span>" + author + "</span> - <span>" + name + "</span>");
	},

	add: function(e){
		alert(this.model.get('nombre'));
	}
});

Sfotipy.Router = Backbone.Router.extend({
	routes: {
		'': 'index',
		'album/:name': 'album',
		'profile/:username': 'profile'
	},

	index: function(){
		...
	},

	album: function(name){
		...
	}

	profile: function(user){
		...
	}
})

Sfotipy.app = new Sfotipy.Router();
Backbone.history.start();

window.Sfotipy = Sfotipy;

*/

/*
// Usando nuestro namespace desde el exterior
var song1 = new Sfotipy.Song({name:"One", author: "Metallica"});
var song2 = new Sfotipy.Song({name:"Fuel", author: "Metallica"});
var songs = new Sfotipy.Songs([song1, song2]);
songs.add(song3);
songs.on('add', function(){...}) //Ejecuta una accion al agregar un elemento a la colectioon
var songView = new Sfotipy.SongView({model: song, el: $('list')});
// el: $('list') es el elemento padre de nuestra vista.

// podemos filtrar modelos de una coleccion
songs.where({name: 'one'})

// navegar por las rutas de mi router
Sfotipy.app.navigate("album/blabla");
*/

/*
*  FIN EJEMPLO DE MEJORANDOELHUEVO
*/




/*
	Prueba desde la consola

sor
child {cid: "c1", attributes: Object, _changing: false, _previousAttributes: Object, changed: Object…}_changing: false_pending: false_previousAttributes: Objectattributes: Object__v: 1_id: "545c0ea706e75716079d8c5d"autoridad: Array[2]estado: "sorteando"fecha: "2014-11-13T00:00:00.000Z"fondo: Objectnro_sorteo: 1rondas: Array[1]tipo_sorteo: Object__proto__: Objectchanged: Objectcid: "c1"__proto__: Surrogate
sor.nro_sorteo
undefined
sor.atributes.nro_sorteo
Uncaught TypeError: Cannot read property 'nro_sorteo' of undefined VM3138:2
sor.atributes
undefined
sor.attributes
Object {_id: "545c0ea706e75716079d8c5d", nro_sorteo: 1, fecha: "2014-11-13T00:00:00.000Z", tipo_sorteo: Object, estado: "sorteando"…}
sor.attributes.nro_sorteo
1
sor.attributes.rondas
[Objectcontenido: Objectbingo: Array[10]0: "6"1: "7"2: "8"3: "9"4: "10"5: "11"6: "12"7: "13"8: "14"9: "15"length: 10__proto__: Array[0]linea: Array[5]__proto__: ObjectnroRonda: 1__proto__: Object]
sor.attributes.rondas.contenido.bingo
Uncaught TypeError: Cannot read property 'bingo' of undefined VM3250:2
sor.attributes.rondas.contenido
undefined
sor.attributes.rondas[0].contenido
Object {bingo: Array[10], linea: Array[5]}
sor.attributes.rondas[0].contenido.bingo
["6", "7", "8", "9", "10", "11", "12", "13", "14", "15"]
sor.attributes.rondas[0].nroRonda
1

*/
